<?php 
require_once("animal.php");
require_once("Ape.php");
require_once("Frog.php");

    $sheep = new Animal("shaun");
    echo "Nama Hewan : $sheep->name <br>"; // "hop hop"
    echo "Jumlah Kaki : $sheep->legs <br>"; // 2
    echo "Suhu tubuh darah dingin : $sheep->cold_blooded <br>"; // false
    echo "<br><br><br>";

    $sungokong = new Ape("sungokong");
    echo "Nama Hewan : $sungokong->name <br>"; // "hop hop"
    echo "Jumlah Kaki : $sungokong->legs <br>"; // 2
    echo "Suhu tubuh darah dingin : $sungokong->cold_blooded <br>"; // false
    echo $sungokong->yell();    
    echo "<br><br>";

    $kodok = new Frog("buduk");
    echo "Nama Hewan : $kodok->name <br>"; // "hop hop"
    echo "Jumlah Kaki : $kodok->legs <br>"; // 2
    echo "Suhu tubuh darah dingin : $kodok->cold_blooded <br>"; // false
    echo $kodok->jump();    
    echo "<br><br>";
?>